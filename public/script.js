var storage = window.localStorage

window.onchange = function() {
    save()
}

window.onkeydown = function(event) {
    if (event.keyCode !== 32) return
    const caller = document.activeElement
    if (caller.classList.contains('button')) {
        caller.click()
        return false
    }
}

function save() {
    let json = JSON.stringify(cardsToJSON())
    storage.setItem('data', json)
}

function load() {
    var data = JSON.parse(storage.getItem('data') || '[]')

    if (!data.length) return addCard({})

    for (var card of data) addCard(card)
}

function cardsToJSON() {
    const cards = document.getElementsByTagName('card')
    var json = []

    for (var card of cards) {
        json.push({
            'name': card.querySelector('.name').value,
            'description': card.querySelector('.description').value,
            'hp': card.querySelector('.health').value,
            'atk': card.querySelector('.attack').value,
            'rarity': card.querySelector('.rarity').value,
            'imageURL': card.querySelector('.imageURL').value
        })
    }
    return json
}

function fitRows(caller) {
    const canvas = fitRows.canvas || (fitRows.canvas = document.createElement("canvas"))
    const context = canvas.getContext("2d")
    context.font = "9.8pt arial"
    const textLength = context.measureText(caller.value).width+0.01
    const boxLength = caller.clientWidth-4

    const rows = Math.ceil(textLength / boxLength)
    caller.rows = rows
}

const colorMap = {
    "COMMON": "#D8D8D8",
    "RARE": "#9FE5ED",
    "SUPER": "#D1A21F",
    "ULTIMATE": "#9E1E9C"
}

function changeImage(caller) {
    const image = caller.parentElement.querySelector('.image')
    image.src = this.caller
}

function changeRarity(caller) {
    const color = colorMap[caller.value]
    caller.parentElement.style.borderLeftColor = color
}

// navbar buttons

function addCard(data) {
    const template = document.getElementsByTagName("template")[0]
    var card = template.content.cloneNode(true)

    if (data.name) card.querySelector('.name').value = data.name

    if (data.description) {
        var description = card.querySelector('.description')
        description.value = data.description
    }

    if (data.hp) card.querySelector('.health').value = data.hp
    if (data.atk) card.querySelector('.attack').value = data.atk

    if (data.imageURL) {
        card.querySelector('.image').src = data.imageURL
        card.querySelector('.imageURL').value = data.imageURL
    }

    var rarity = card.querySelector('.rarity')
    rarity.value = data.rarity || 'COMMON'
    changeRarity(rarity)

    document.getElementsByTagName('container')[0].appendChild(card)

    if (data.description) fitRows(description)
    save()
}

function downloadCards() {
    var json = cardsToJSON()
    if (!json) return
    var json = JSON.stringify(json) // null, 2

    let download = document.createElement('a')
    download.href = 'data:text/plaincharset=utf-8,' + encodeURIComponent(json)
    download.setAttribute('download', 'cards.json')
    download.style.display = 'none'

    document.body.appendChild(download)
    download.click()
    document.body.removeChild(download)
}

function importCards(caller) {
    const file = caller.files[0]
    const reader = new FileReader()
    reader.readAsText(file, "UTF-8")
    reader.onload = function (evt) {
        const data = evt.target.result
        const json = JSON.parse(data)
        if (!json) return
        const check = confirm("Do you want to reset your current cards?")
        if (check) document.getElementsByTagName('container')[0].innerHTML = ''
        for (var card of json) addCard(card)
        save()
    }
    reader.onerror = function () {
        console.log("Error reading file!")
    }
}

function resetCards() {
    const check = confirm("Are you sure that you want to reset your cards? \nThere is no way to recover them unless you saved them locally")
    if (check) {
        document.body.querySelector('container').innerHTML = ''
        storage.removeItem('data')
    }
}

// other buttons

function deleteCard(event, card) {
    if (!event.shiftKey) {
        const check = confirm("Are you sure that you want to delete this card?\nTip: You can skip this message by holding Shift")
        if (!check) return false
    }
    card.parentElement.remove()
    save()
}

function uploadImage(caller) {
    var apiKey = storage.getItem('apiKey')
    if (!apiKey) {
        var apiKey = prompt("You need an ImgBB API Key in order to upload images directly\nTo get one, go to https://api.imgbb.com/ and request one")
        if (!apiKey) return
        storage.setItem('apiKey', apiKey)
    }

    var reader = new FileReader()

    reader.onloadend = function() {
        let b64 = reader.result.replace(/^data:.+base64,/, '').replaceAll('=', '%3D').replaceAll('+', '%2B')
        var xhr = new XMLHttpRequest()
        var url = 'https://api.imgbb.com/1/upload?key=' + storage.getItem('apiKey')

        xhr.onreadystatechange = function () {
            if (xhr.readyState !== XMLHttpRequest.DONE) return

            if (xhr.status == 400) switch(JSON.parse(xhr.response).error.code) {
                case 100:
                    alert("Invalid API key")
                    storage.removeItem('APIkey')
                    return
                case 102:
                    alert("Invalid Base64 string")
                    return
            }
            else if (xhr.status == 200) {
                const parent = caller.parentElement.parentElement
                const url = JSON.parse(xhr.response).data.display_url.replace('+', '%2B')

                parent.querySelector('.image').src = url
                parent.querySelector('.imageURL').value = url
                save()
            }

        }

        xhr.open("POST", url)
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
        xhr.send('image=' + b64.replace('+', ' '))
    
    }
    
    reader.readAsDataURL(caller.files[0])
}